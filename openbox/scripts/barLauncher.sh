#!/usr/bin/env sh

# Terminate already running bar instances
killall -q tint2

# Wait until the processes have been shut down
while pgrep -u $UID -x tint2 >/dev/null; do sleep 1; done

# Launch main bar
tint2 &
