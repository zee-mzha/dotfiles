#!/bin/bash
#TMPBG=/tmp/screen.png
#LOCK=$HOME/.config/i3/scripts/lock.png
#RES=1920x1080
 
#ffmpeg -f x11grab -video_size $RES -y -i $DISPLAY -i $LOCK -filter_complex "boxblur=5:1,overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2" -vframes 1 $TMPBG -loglevel quiet
i3lock -e -B 1 --indicator --indpos="210:h-100" --radius 40 --ring-width 3 --veriftext=""\
	--wrongtext="" --noinputtext="" --locktext="" --lockfailedtext="" --insidecolor=00000000\
	--linecolor=00000000 --ringcolor=66beb2ff --keyhlcolor=b2eee6ff --separatorcolor=b2eee6ff\
	--insidevercolor=8ad6ccff --ringvercolor=8ad6ccff --insidewrongcolor=f97171ff --ringwrongcolor=f99192ff\
	-k --timepos="ix-125:iy" --timestr="%H:%M" --timecolor=ffffffff --datestr="%a, %d %b" --datecolor=ffffffff
#rm $TMPBG